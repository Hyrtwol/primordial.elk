﻿#if !REDIS
using System;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using NUnit.Framework;
using Primordial.ELK.Test;

namespace Primordial.ELK.MSBuild.Test
{
#if IGNORE_UNIT_TEST
    [Ignore("TeamCity is unable to connect to redis")]
#endif
    [TestFixture]
    public class ElasticsearchNodeLoggerTests
    {
        [Test]
        public void LogBuild()
        {
#pragma warning disable 612, 618
            var engine = new Engine();
#pragma warning restore 612, 618

            try
            {
                ILogger logger = new ElasticsearchNodeLogger();
                logger.Parameters = "elsurl=" + TestSettings.ElasticsearchUrl +
                                    ";type=primordial-elk-msbuild-test;index=unittest";
                engine.RegisterLogger(logger);

                var project = engine.CreateNewProject();

                project.DefaultTargets = "Build";

                Assert.IsNotNull(project.Targets);
                var buildTarget = project.Targets.AddNewTarget("Build");

                var task = buildTarget.AddNewTask("Message");
                task.SetParameterValue("Text", "MSBuild fake error " + DateTime.Now.Ticks);
                task.SetParameterValue("Importance", "high");

                Assert.IsTrue(project.Build());
            }
            finally
            {
                engine.UnloadAllProjects();
                engine.UnregisterAllLoggers();
                engine.Shutdown();
            }
        }

    }
}
#endif