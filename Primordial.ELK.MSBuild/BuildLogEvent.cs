﻿using Newtonsoft.Json;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.MSBuild
{
    public class BuildLogEvent : LogEvent
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object build;
    }
}