﻿#if !NOGO
using System;
using System.Diagnostics;
using Microsoft.Build.Framework;
using Primordial.ELK.Elasticsearch;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.MSBuild
{
    public class ElasticsearchNodeLogger : INodeLogger
    {
        private static readonly char[] FileLoggerParameterDelimiters = {';'};
        private static readonly char[] FileLoggerParameterValueSplitCharacter = {'='};

// ReSharper disable NotAccessedField.Local
        private int _numberOfProcessors;
        // ReSharper restore NotAccessedField.Local

        //private Encoding _encoding = Encoding.Default;
        private string _loggerName = "MSBuild";
        private string _logType = nameof(ElasticsearchNodeLogger);
        private string _elasticsearchUrl;
        //private string _indexType;
        private string _indexName;
        private ElasticsearchClient _elsClient;

        public LoggerVerbosity Verbosity { get; set; }
        public string Parameters { get; set; }

        public ElasticsearchNodeLogger()
        {
            _elasticsearchUrl = "http://localhost:9200";
            _indexName = $"build-{DateTime.UtcNow:yyyy.MM.dd}";
        }

        public void Initialize(IEventSource eventSource, int nodeCount)
        {
            this.InitializeFileLogger(eventSource, nodeCount);
        }

        public void Initialize(IEventSource eventSource)
        {
            this.InitializeFileLogger(eventSource, 1);
        }

        private void InitializeFileLogger(IEventSource eventSource, int nodeCount)
        {
            _numberOfProcessors = nodeCount;

            if (eventSource == null) return;

            //string parameters = base.Parameters;
            //if (parameters != null)
            //{
            //    base.Parameters = "FORCENOALIGN;" + parameters;
            //}
            //else
            //{
            //    base.Parameters = "FORCENOALIGN;";
            //}

            this.ParseFileLoggerParameters();
            //base.Initialize(eventSource, nodeCount);

            Debug.Print("Create ELS Client {0}", _elasticsearchUrl);
            if (_elsClient != null) throw new InvalidOperationException("Redis ElasticsearchClient already initialized.");
            var elsuri = new Uri(_elasticsearchUrl);
            _elsClient = new ElasticsearchClient(elsuri, _indexName);

            eventSource.BuildStarted += LogToELS;
            eventSource.BuildFinished += LogToELS;
            eventSource.ProjectStarted += LogToELS;
            eventSource.ProjectFinished += LogToELS;
            //eventSource.ProjectStarted += ProjectStartedHandler;
            //eventSource.ProjectFinished += ProjectFinishedHandler;
            //eventSource.TargetStarted += TargetStartedHandler;
            //eventSource.TargetFinished += TargetFinishedHandler;
            //eventSource.TaskStarted += TaskStartedHandler;
            //eventSource.TaskFinished += TaskFinishedHandler;
            eventSource.ErrorRaised += LogToELS;
            eventSource.WarningRaised += LogToELS;
            eventSource.MessageRaised += LogToELS;
            //eventSource.CustomEventRaised += CustomEventHandler;
        }

        public void Shutdown()
        {
#if DEBUG
            Debug.WriteLine("LogstashNodeLogger.Shutdown()");
#endif
            _elsClient = null;
        }

        private void ParseFileLoggerParameters()
        {
            if (this.Parameters != null)
            {
                string[] strArray = this.Parameters.Split(FileLoggerParameterDelimiters);
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i].Length > 0)
                    {
                        string[] strArray2 = strArray[i].Split(FileLoggerParameterValueSplitCharacter);
                        if (strArray2.Length > 1)
                        {
                            this.ApplyFileLoggerParameter(strArray2[0], strArray2[1]);
                        }
                        else
                        {
                            this.ApplyFileLoggerParameter(strArray2[0], null);
                        }
                    }
                }
            }
        }

        private void ApplyFileLoggerParameter(string parameterName, string parameterValue)
        {
            var str = parameterName.ToUpperInvariant();
            if (string.IsNullOrEmpty(str)) return;
            switch (str)
            {
                case "ELSURL":
                    _elasticsearchUrl = parameterValue;
                    return;
                //case "PORT":
                //    if (!int.TryParse(parameterValue, out _redisPort)) _redisPort = RedisClient.DefaultPort;
                //    return;
                //case "TYPE":
                //    _indexType = parameterValue;
                //    return;
                case "INDEX":
                    _indexName = $"{parameterValue}-{DateTime.UtcNow:yyyy.MM.dd}";
                    return;
                case "LOGGERNAME":
                    _loggerName = parameterValue;
                    return;
            }
        }

        // --------------------------------------------

        private void BuildStartedHandler(object sender, BuildStartedEventArgs e)
        {
            LogToELS(sender,e);
        }

        private void BuildFinishedHandler(object sender, BuildFinishedEventArgs e)
        {
            LogToELS(sender,e);
        }

        private void MessageHandler(object sender, BuildMessageEventArgs e)
        {
            LogToELS(sender,e);
        }

        private void LogToELS(object sender, BuildEventArgs e)
        {
            if (_elsClient == null) return;
            var logEvent = CreateLogEvent(e);
            if (logEvent == null) return;
            _elsClient.SendMessage(logEvent);
        }

        private void ProjectStartedHandler(object sender, ProjectStartedEventArgs e)
        {
            WriteLog("ProjectStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ProjectFinishedHandler(object sender, ProjectFinishedEventArgs e)
        {
            WriteLog("ProjectFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetStartedHandler(object sender, TargetStartedEventArgs e)
        {
            WriteLog("TargetStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetFinishedHandler(object sender, TargetFinishedEventArgs e)
        {
            WriteLog("TargetFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskStartedHandler(object sender, TaskStartedEventArgs e)
        {
            WriteLog("TaskStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskFinishedHandler(object sender, TaskFinishedEventArgs e)
        {
            WriteLog("TaskFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ErrorHandler(object sender, BuildErrorEventArgs e)
        {
            WriteLog("ErrorRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void WarningHandler(object sender, BuildWarningEventArgs e)
        {
            WriteLog("WarningRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void CustomEventHandler(object sender, CustomBuildEventArgs e)
        {
            WriteLog("CustomEventRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private bool IsVerbosityAtLeast(LoggerVerbosity checkVerbosity)
        {
            return (Verbosity >= checkVerbosity);
        }

        private void WriteLog(string action, string format, params object[] arg)
        {
            Console.Write("{0,-20} :: ", action);
            Console.WriteLine(format, arg);
        }

        private string CreateJson(BuildEventArgs logEvent)
        {
            var messageEvent = CreateLogEvent(logEvent);
            return messageEvent.ToJson();
        }

        private BuildLogEvent CreateLogEvent(BuildEventArgs logEvent)
        {
            var messageEvent = new BuildLogEvent();
            messageEvent.timestamp = logEvent.Timestamp;
            messageEvent.AddLogInfo(MessageImportance.High.ToString(), _loggerName, _logType);
            messageEvent.message = logEvent.Message;
            messageEvent.build = logEvent;
            return messageEvent;
        }

    }
}
#endif