﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.NLog
{
    [Target("Elasticsearch")]
    public sealed class ElasticsearchTarget : TargetWithLayout
    {
        public const string DefaultIndex = "logstash-${date:format=yyyy.MM.dd}";

        [RequiredParameter]
        public string ElasticsearchUrl { get; set; }
        
        //[RequiredParameter]
        [DefaultValue(DefaultIndex)]
        public Layout Index { get; set; } = DefaultIndex;

        protected override void InitializeTarget()
        {
            base.InitializeTarget();
            //if (Index == null)
            //{
            //    Index = new SimpleLayout(DefaultIndex);
            //}
        }

        protected override void CloseTarget()
        {
            base.CloseTarget();
        }

        protected override void Dispose(bool disposing)
        {
            //Debug.WriteLine("ElasticsearchTarget.Dispose()");
            base.Dispose(disposing);
        }

        protected override void Write(LogEventInfo logEvent)
        {
            if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));
            
            
            var elasticsearchUri = ElasticsearchClient.CreateUriFromUrl(ElasticsearchUrl);
            var indexName = GetIndexName(logEvent);
            var elasticsearchClient = new ElasticsearchClient(elasticsearchUri, indexName);
            var json = GetJson(logEvent);
            elasticsearchClient.SendMessage(json);
        }

        private string GetJson(LogEventInfo logEvent)
        {
            var json = Layout.Render(logEvent);
            return json;
        }

        private string GetIndexName(LogEventInfo logEvent)
        {
            var indexName = Index.Render(logEvent);
            InternalLogger.Log(LogLevel.Info, "index name: " + indexName);
            if (string.IsNullOrEmpty(indexName))
            {
                indexName = string.Format("logstash-{0:yyyy.MM.dd}", DateTime.UtcNow);
            }
            return indexName;
        }
    }
}
