using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.Elasticsearch
{
    //public class ElasticsearchClient<T> : ElasticsearchClient where T : LogEvent
    //{
    //    public ElasticsearchClient(Uri elasticsearchUri) : base(elasticsearchUri)
    //    {
    //        var _typeOfT = typeof(T);
    //        IndexType = _typeOfT.FullName;
    //    }

    //    public void SendMessage(T logEvent)
    //    {
    //        base.SendMessage(logEvent);
    //    }
    //}

    public class ElasticsearchClient
    {
        protected readonly Uri _elasticsearchUrl;

        public int ElasticsearchMajorVersion = 5;

        public string IndexName = "logstash";
        public string IndexType;

        // see https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html 
        // 5.x: PUT {index}/{type}/{id} and POST {index}/{type}
        // 6.x: PUT {index}/_doc/{id} and POST {index}/_doc
        // 7.x: PUT {index}/_doc/{id} and POST {index}/_doc

        public ElasticsearchClient(Uri elasticsearchUri, string indexName)
        {
            _elasticsearchUrl = elasticsearchUri ?? throw new ArgumentNullException(nameof(elasticsearchUri));
            if(string.IsNullOrEmpty(indexName)) throw new ArgumentNullException(nameof(indexName));
            IndexName = indexName;
        }

        public void SendMessage(LogEvent logEvent)
        {
            SendMessage(logEvent.ToJson());
        }

        public void SendMessage(string json)
        {
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException(nameof(json));
            //Debug.WriteLine(json);
            var elasticsearchIndexUrl = CreateElasticsearcIndexUrl();
            Send(elasticsearchIndexUrl, json);
        }

        public Uri CreateElasticsearcIndexUrl()
        {
            string index = IndexName;
            if (string.IsNullOrEmpty(index))
            {
                index = string.Format("logstash-{0:yyyy.MM.dd}", DateTime.UtcNow);
            }
            string type;
            if (ElasticsearchMajorVersion > 5)
            {
                type = "_doc";
            }
            else
            {
                type = IndexType;
                if (string.IsNullOrEmpty(type)) type = "doc";
            }
            var id = Guid.NewGuid().ToString("N");

            return CreateElasticsearcIndexUrl(index, type, id);
        }

        public Uri CreateElasticsearcIndexUrl(string index, string type, string id)
        {
            if (string.IsNullOrEmpty(index)) throw new ArgumentNullException("index");
            if (string.IsNullOrEmpty(type)) throw new ArgumentNullException("type");
            if (string.IsNullOrEmpty(id)) throw new ArgumentNullException("id");
            return new Uri(_elasticsearchUrl, Path.Combine(index, type, id));
        }

        private static void Send(Uri elasticsearchIndexUrl, string json)
        {
            if (elasticsearchIndexUrl == null) throw new ArgumentNullException("elasticsearchIndexUrl");
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException("json");
#if DEBUG
            Debug.Print("elasticsearchIndexUrl={0}", elasticsearchIndexUrl);
#endif
            var webRequest = WebRequest.Create(elasticsearchIndexUrl);
            webRequest.Method = "POST";
            webRequest.Timeout = 5000; // in ms so 5 seconds
            webRequest.ContentType = "application/json";

            using (var s = webRequest.GetRequestStream())
            using (var sw = new StreamWriter(s))
                sw.Write(json);

            using (var response = (HttpWebResponse)webRequest.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.Created) throw new Exception("log entry was not created successfully in elasticsearch");
            }
        }

        public static Uri CreateUriFromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentNullException("url");
            Uri elasticsearchUri;
            if (Uri.TryCreate(url, UriKind.Absolute, out elasticsearchUri))
            {
                return elasticsearchUri;
            }
            var message = "Unable to create uri from " + url;
#if DEBUG
            Debug.WriteLine("ERROR: " + message);
#endif
            throw new InvalidOperationException(message);
        }
    }
}