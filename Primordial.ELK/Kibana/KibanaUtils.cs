using Newtonsoft.Json;

namespace Primordial.ELK.Kibana
{
    public static class KibanaUtils
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ContractResolver = new KibanaPropertyNamesContractResolver(),
            Formatting = Formatting.Indented,
            NullValueHandling = NullValueHandling.Ignore
        };

        public static string ToJson(this LogEvent logEvent)
        {
            return JsonConvert.SerializeObject(logEvent, Settings);
        }

    }
}