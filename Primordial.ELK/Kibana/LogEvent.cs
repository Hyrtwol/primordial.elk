using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedField.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CollectionNeverQueried.Global

namespace Primordial.ELK.Kibana
{
    public class LogEvent
    {
        [JsonProperty(PropertyName = "@timestamp")]
        //[JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime timestamp = DateTime.Now;
        
        public HostEnvironment environment = HostEnvironment.Create();

        public Application application = Application.Create();
        
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Logger log;
        
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public HashSet<string> tags;
        
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object exception;

        //public object properties = new object();
        public Dictionary<string, object> properties;
        
        public string message;

        public void AddException(Exception ex)
        {
            if (ex == null) return;
            exception =
                new
                {
                    message = ex.Message,
                    stacktrace = ex.StackTrace
                };
        }

        public void AddProperty(string key, object value)
        {
            if (properties == null) properties = new Dictionary<string, object>();
            try
            {
                properties.Add(key, value);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException(ex.Message + " key=" + key, ex);
            }
        }

        public void AddTags(params string[] tag)
        {
            if(tags == null) tags = new HashSet<string>();
            foreach (var t in tag)
            {
                tags.Add(t);
            }
        }

        public void AddLogInfo(string level, string name, string type)
        {
            log = new Logger
            {
                level = level,
                name = name,
                type = type
            };
        }

        public void AddStackFrame(StackFrame stackFrame)
        {
            if (stackFrame == null) return;
            var method = stackFrame.GetMethod();
            AddProperty("stackframe",
                new
                {
                    path = stackFrame.GetFileName(),
                    line = stackFrame.GetFileLineNumber(),
                    method = method.Name,
                    @class = method.DeclaringType?.ToString()
                });
        }

    }

    public class HostEnvironment
    {
        public string name;
        public string user;
        public string os;
        public int cpu;
        public long workingset;
        public bool x64;
        //public string[] ips;
        public string ips;

        public static HostEnvironment Create()
        {
            var host = new HostEnvironment();
            host.name = Environment.MachineName;
            host.user = Environment.UserName;
            host.os = Environment.OSVersion.ToString();
            host.x64 = Environment.Is64BitOperatingSystem;
            host.cpu = Environment.ProcessorCount;
            host.workingset = Environment.WorkingSet >> 10;
            host.ips = string.Join(";", GetIps());
            return host;
        }

        private static IEnumerable<string> GetIps()
        {
            return Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .Select(ip => ip.ToString());
        }
    }

    public class Application
    {
        public string name;
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string version;
        public string domain;
        public long gcmem;
        //public string codebase;

        public static Application Create()
        {
            return Create(Globals.GetAssemblyName());
        }

        public static Application Create<T>()
        {
            return Create(typeof(T).Assembly?.GetName());
        }

        private static Application Create(System.Reflection.AssemblyName an)
        {
            var app = new Application();

            var domain = AppDomain.CurrentDomain;
            //app.basepath = domain.BaseDirectory;
            app.domain = domain.FriendlyName;
            app.gcmem = GC.GetTotalMemory(false) >> 10;

            if (an != null)
            {
                app.name = an.Name;
                app.version = an.Version.ToString();
            }
#if !NETSTANDARD
            else
            {
                var setupInformation = domain.SetupInformation;
                app.name = setupInformation.ApplicationName;
            }
#endif
            return app;
        }
    }

    public class Logger
    {
        public string level;
        public string name;
        public string type;
    }
}