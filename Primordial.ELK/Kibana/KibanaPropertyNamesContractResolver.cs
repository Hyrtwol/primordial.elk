﻿using Newtonsoft.Json.Serialization;

namespace Primordial.ELK.Kibana
{
    public class KibanaPropertyNamesContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return ToKibanaField(propertyName);
        }

        public static string ToKibanaField(string s)
        {
            return string.IsNullOrEmpty(s) ? null : s.ToLowerInvariant();
        }
    }

}