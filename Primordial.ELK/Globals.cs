using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace Primordial.ELK
{
    public static class Globals
    {
        public const int DefaultElasticsearchPort = 9200;
        public const int DefaultRedisPort = 6379;

        private static AssemblyName _applicationAssemblyName;

        public static void SetApplicationAssembly<T>()
        {
            SetApplicationAssembly(typeof (T).Assembly);
        }

        public static void SetApplicationAssembly(Assembly assembly)
        {
            if(assembly==null) throw new ArgumentNullException("assembly");
            _applicationAssemblyName = assembly.GetName();
        }

        public static readonly string OsVersion = Environment.OSVersion.ToString();
        
        public static readonly JArray HostIps = new JArray(Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(ip => (object)ip.ToString())
            .ToArray());

        public static AssemblyName GetAssemblyName()
        {
            if (_applicationAssemblyName == null)
            {
                var asm = Assembly.GetEntryAssembly();
                if (asm != null)
                {
                    _applicationAssemblyName = asm.GetName();
                }
                else
                {
                    asm = Assembly.GetExecutingAssembly();
                    _applicationAssemblyName = asm.GetName();
                }
                _applicationAssemblyName = Assembly.GetExecutingAssembly().GetName();
            }
            return _applicationAssemblyName;
        }

        public static string GetApplicationName(this AssemblyName assemblyName)
        {
            return assemblyName != null ? assemblyName.Name.Replace('.', '-').ToLowerInvariant() : "N/A";
        }
    }
}