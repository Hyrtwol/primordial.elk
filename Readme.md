# Primordial.ELK

The ELK stack is ElasticSearch, Logstash and Kibana.

## Packages

[ELK Stack Downloads](https://www.elastic.co/downloads)

### Primordial.ELK

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK/)

ElasticSearch, Logstash and Kibana

### Primordial.ELK.NLog

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.nlog.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK.NLog/)

ELK for NLog

* [NLog Targets](https://github.com/NLog/NLog/wiki/Targets)
* [NLog Layout Renderers](https://github.com/NLog/NLog/wiki/Layout-Renderers)

Config example:

    <target name="elk"
            xsi:type="Elasticsearch"
            elasticsearchUrl="http://localhost:9200"
            index="logstash-${date:format=yyyy.MM.dd}">
      <layout xsi:type="Kibana">
        <text>${message}</text>
      </layout>
    </target>

### Primordial.ELK.log4net

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.log4net.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK.log4net/)

ELK for log4net

Config example:

    <appender name="elk" type="Primordial.ELK.log4net.ElasticsearchAppender, Primordial.ELK.log4net">
      <param name="ElasticsearchUrl" value="http://localhost:9200"/>
      <layout type="Primordial.ELK.log4net.KibanaLayout, Primordial.ELK.log4net">
        <conversionPattern value="%m%n" />
      </layout>
    </appender>

### Primordial.ELK.MSBuild

ELK for MSBuild

