﻿<Project DefaultTargets="All" ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">

  <PropertyGroup>
    <RecipeVersion>5.1</RecipeVersion>
    <PublishVersion>$(RecipeVersion).21</PublishVersion>
    <ArtifactName>Primordial.ELK</ArtifactName>
    <ArtifactSolution>Primordial.ELK</ArtifactSolution>
    <ArtifactCompany>Hyrtwol</ArtifactCompany>
    <ArtifactCopyrightYear>2015</ArtifactCopyrightYear>
    <ArtifactAuthors>Thomas la Cour</ArtifactAuthors>
    <ArtifactCopyright>Copyright $(ArtifactCompany) 2015</ArtifactCopyright>
    <ArtifactProjectUrl>https://bitbucket.org/Hyrtwol/primordial.elk</ArtifactProjectUrl>
    <ArtifactVCSRootUrl>https://bitbucket.org/Hyrtwol/primordial.elk.git</ArtifactVCSRootUrl>
    <ArtifactIconUrl>http://hyrtwol.dk/icons/primordial.ico</ArtifactIconUrl>
    <ArtifactLicenseUrl>http://hyrtwol.dk/primordial/mit.txt</ArtifactLicenseUrl>
    <UnitTestForce32Bit>true</UnitTestForce32Bit>
  </PropertyGroup>

  <ItemGroup>
    <Project Include="Primordial.ELK">
      <Flavour>NuGet</Flavour>
      <Description>ElasticSearch, Logstash and Kibana</Description>
    </Project>
    <Project Include="Primordial.ELK.log4net">
      <Flavour>NuGet</Flavour>
      <Description>ELK for log4net 1.2.10</Description>
    </Project>
    <!--<Project Include="Primordial.ELK.MSBuild">
      <Flavour>NuGet</Flavour>
      <Description>ELK for MSBuild</Description>
    </Project>-->
    <Project Include="Primordial.ELK.NLog">
      <Flavour>NuGet</Flavour>
      <Description>ELK for NLog 4.1.2</Description>
    </Project>
  </ItemGroup>

  <!-- <Import Project="$(Pantry)\Fry.targets"/> -->
  <Import Project="$(Pantry)\DotNetCore.targets"/>
  <Import Project="$(Pantry)\Cuisine.NuGet.targets"/>

  <!--
  <PropertyGroup>
    <DeployDependsOn>$(DeployDependsOn);NuGetDeployPackagesDependencies</DeployDependsOn>
  </PropertyGroup>
  -->
  <ItemGroup>
    <!-- <PrepareGoal Include="DotNetRestore" /> -->
    <DeployGoal Include="DeployExtraNuGetPackages;NuGetDeployPackagesDependencies" />
  </ItemGroup>

  <Target Name="Test" />
  <Target Name="Pack" />
  <Target Name="DotNetDeploy" />

  <Target Name="DeployExtraNuGetPackages">
    <ItemGroup>
      <NupkgDependency Include="**\Release\*.$(BuildVersion).nupkg" />
    </ItemGroup>
    <Message Text="NupkgDependency=%(NupkgDependency.Identity)" Importance="high" />
    <Error Condition="!Exists('%(NupkgDependency.Identity)')" Text="Missing file %(NupkgDependency.Identity)" />
    <NuGetPush PackagePath="%(NupkgDependency.Identity)" Source="$(NuGetPublishUrl)" ApiKey="$(NuGetApiKey)" Verbosity="Detailed" CommandLineVersion="$(NugetCommandLineVersion)" />
  </Target>

  <Target Name="Publish" DependsOnTargets="Clean">

    <PropertyGroup>
      <!-- local myget nugetorg -->
      <PublishTo>nugetorg</PublishTo>
      <PublishWhatIf>false</PublishWhatIf>
      <PublishSourceFeed>$(KlondikeOntogenesisFeedUrl)</PublishSourceFeed>
    </PropertyGroup>

    <Message Text="Publishing version $(PublishVersion) to '$(PublishTo)'" />
    <Message Text="Using source feed '$(PublishSourceFeed)'" />

    <Error Condition="'$(PublishTo)'==''" Text="PublishTo is not defined" />
    <Error Condition="'$(PublishSourceFeed)'==''" Text="PublishSourceFeed is not defined" />

    <RemoveDir Directories="$(ArtifactsFolder)" Condition=" Exists('$(ArtifactsFolder)') " />
    <MakeDir Directories="$(ArtifactsFolder)" />

    <NuGetInstall Source="$(PublishSourceFeed)"
                  PackagesConfig="%(Project.Identity)"
                  OutputDirectory="$(ArtifactsFolder)"
                  Verbosity="detailed"
                  Version="$(PublishVersion)"
                  ExcludeVersion="false"
                  NoCache="true"
                  Condition=" '%(Project.Flavour)' == 'NuGet' " />

    <ItemGroup>
      <Nupkg Include="$(ArtifactsFolder)\**\$(ArtifactSolution)*.nupkg" />
    </ItemGroup>

    <Message Text="Nupkg=%(Nupkg.Identity)" Importance="high" />
    <Error Condition="'@(Nupkg)'==''" Text="Nothing to publish searching '$(ArtifactsFolder)\**\$(ArtifactSolution)*.nupkg'" />
    <Error Condition="!Exists('%(Nupkg.Identity)')" Text="Missing file %(Nupkg.Identity)" />

    <PropertyGroup Condition=" '$(PublishTo)' == 'local' ">
      <_NuGetPublishUrl>$(NuGetPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(NuGetApiKey)</_NuGetApiKey>
    </PropertyGroup>
    <PropertyGroup Condition=" '$(PublishTo)' == 'myget' ">
      <_NuGetPublishUrl>$(MyGetPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(MyGetApiKey)</_NuGetApiKey>
    </PropertyGroup>
    <PropertyGroup Condition=" '$(PublishTo)' == 'nugetorg' ">
      <_NuGetPublishUrl>$(NuGetOrgPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(NuGetOrgApiKey)</_NuGetApiKey>
    </PropertyGroup>

    <Message Text="Pushing to '$(_NuGetPublishUrl)' with apikey '$(_NuGetApiKey)'" />

    <NuGetPush PackagePath="%(Nupkg.Identity)"
               Source="$(_NuGetPublishUrl)"
               ApiKey="$(_NuGetApiKey)"
               Verbosity="detailed"
               Condition="!$(PublishWhatIf)" />

    <Message Text="WhatIf: %(Nupkg.Identity)" Condition="$(PublishWhatIf)" />

  </Target>

</Project>