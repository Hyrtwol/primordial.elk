﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;
using log4net.Appender;
using log4net.Config;
using NUnit.Framework;

namespace Primordial.ELK.log4net.Test
{
    [TestFixture]
    public class ConfigFileTests
    {
        [TestCase(@"testdata\log4net-logstash-elasticsearch-dev.config")]
        public void LogError(string configFileName)
        {
            Configure(configFileName);

            // http://www.beefycode.com/post/Log4Net-Tutorial-pt-6-Log-Event-Context.aspx
            ThreadContext.Properties["contextId"] = 12345678L;

            //log4net.Layout.XmlLayout

            ILog logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            IAppender[] appenders = logger.Logger.Repository.GetAppenders();
            foreach (var appender in appenders)
            {
                Debug.WriteLine(appender);
            }

            for (int i = 0; i < 3; i++)
            {
                logger.WarnFormat("Warning {0}", i);
            }
            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                logger.Error("Log4Net fake error " + DateTime.Now.Ticks, ex);
            }
        }

        private static void Configure(string configFileName)
        {
            using (var stream = File.OpenRead(configFileName))
            {
                XmlConfigurator.Configure(stream);
            }
        }
    }
}
