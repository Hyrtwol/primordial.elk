﻿using System;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
#if LOG4NET
using System.IO;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Repository.Hierarchy;
using log4net.Util;
using Primordial.ELK.log4net;
using LogEventInfo = log4net.Core.LoggingEvent;
using LogLevel = log4net.Core.Level;
#else
using NLog.Config;
using NLog.Targets;
using Primordial.ELK.NLog;
using LogEventInfo = NLog.LogEventInfo;
using LogLevel = NLog.LogLevel;
using LogManager = NLog.LogManager;
using GlobalDiagnosticsContext = NLog.GlobalDiagnosticsContext;
using MappedDiagnosticsContext = NLog.MappedDiagnosticsContext;
#endif

#if LOG4NET
namespace Primordial.ELK.log4net.Test
#else
namespace Primordial.ELK.NLog.Test
#endif
{
    [TestFixture]
    public class KibanaLayoutTests
    {
        private const string ExpectedLogType = "NLog";

        [Test]
        public void RenderOutput()
        {
            const string expectedLogName = "-logger-name-";
            var expectedContextId = Stopwatch.GetTimestamp();
            var timestamp = DateTime.UtcNow;
            string expectedLogLevel, pattern, json;
#if LOG4NET
            expectedLogLevel ="INFO";
            pattern = @"%logger (%property{context_id}) [%level] :: %message";
#else
            expectedLogLevel = "Info";
            pattern = @"${logger} (${event-properties:item=context_id}) [${level}] :: ${message}";
#endif

            var expectedMessage = expectedLogName + " (" + expectedContextId + ") [" + expectedLogLevel + "] :: Just a test";

#if LOG4NET
            var layout = new KibanaLayout(pattern);
            var data = new LoggingEventData
            {
                Message = "Just a test",
                Level = Level.Info,
                LoggerName = expectedLogName,
                Identity = "-identity-",
                LocationInfo = new LocationInfo("-class-", "-method-", "-path-", "666"),
                ThreadName = "-thread-name-",
                TimeStamp = timestamp,
                UserName = "-user-",
                Domain = "-domain-",
                Properties = new PropertiesDictionary {["context_id"] = expectedContextId}
            };
            var loggingEvent = new LoggingEvent(data);
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                layout.Format(writer, loggingEvent);
            }
            json = sb.ToString();
#else
            var layout = new KibanaLayout(pattern, "context_id");
            var loggingEvent = new LogEventInfo
            {
                Message = "Just a test",
                Level = LogLevel.Info,
                LoggerName = expectedLogName,
                TimeStamp = timestamp
            };
            loggingEvent.Properties["context_id"] = expectedContextId;
            json = layout.Render(loggingEvent);
#endif

            Debug.WriteLine(json);
            var jObject = (JObject)JsonConvert.DeserializeObject(json);

            Assert.AreEqual(expectedMessage, (string) jObject["message"]);
            Assert.AreEqual(timestamp, (DateTime)jObject["@timestamp"]);
            Assert.AreEqual(Environment.MachineName, (string)jObject["environment"]["name"]);
            Assert.AreEqual(expectedLogLevel, jObject["log"]["level"].ToString());
            Assert.AreEqual("-logger-name-", jObject["log"]["name"].ToString());
            var fields = jObject["properties"];
            Assert.IsNotNull(fields);
            Assert.AreEqual(expectedContextId, (long)fields["context_id"]);

        }

        [Test]
        public void LogWarningWithCorrelationId()
        {
            const string logName = "UnitTestLogWarningWithCorrelationId";
            var expectedCorrelationId = Guid.NewGuid().ToString("N");
            var expectedContextId = Stopwatch.GetTimestamp();

            var sbt = ConfigureLogging();
            var logger = LogManager.GetLogger(logName);
            Assert.IsNotNull(logger);

#if LOG4NET
            ThreadContext.Properties["correlation_id"] = expectedCorrelationId;
            ThreadContext.Properties["context_id"] = expectedContextId.ToString();
#else
            GlobalDiagnosticsContext.Set("correlation_id", expectedCorrelationId);
            MappedDiagnosticsContext.Set("context_id", expectedContextId);
#endif
            logger.Warn("Just testing the logger with a warning.");

            var logOutput = sbt.GetText();
            Debug.Write(logOutput);

            var obj = JsonConvert.DeserializeObject<JObject>(logOutput);
            Assert.AreEqual("Warn", (string) obj["log"]["level"]);
            Assert.AreEqual(logName, (string) obj["log"]["name"]);
            Assert.AreEqual(ExpectedLogType, (string) obj["log"]["type"]);
            Assert.AreEqual(expectedCorrelationId, (string) obj["properties"]["correlation_id"]);
            Assert.AreEqual(expectedContextId, (long) obj["properties"]["context_id"]);
        }

        [Test]
        public void LogWarningUsingLogEventInfo()
        {
            var expectedContextId = Stopwatch.GetTimestamp();

            var sbt = ConfigureLogging();
            var logger = LogManager.GetLogger("this-name-is-not-used");
            Assert.IsNotNull(logger);

#if LOG4NET
            var data = new LoggingEventData
            {
                Level = Level.Warn, LoggerName = "this-name-is-used",
                Message = "Just testing the logger with a warning."
            };
            var properties = new PropertiesDictionary();
            properties["context_id"] = expectedContextId;
            data.Properties = properties;
            var loggingEvent = new LoggingEvent(data);
            logger.Warn(loggingEvent);
#else
            
            var loggingEvent = new LogEventInfo(LogLevel.Warn, "this-name-is-used",
                "Just testing the logger with a warning.");
            loggingEvent.Properties["context_id"] = expectedContextId;
            logger.Log(loggingEvent);
#endif

            var logOutput = sbt.GetText();
            Debug.Write(logOutput);

            var obj = JsonConvert.DeserializeObject<JObject>(logOutput);
            Assert.AreEqual("Warn", (string) obj["log"]["level"]);
            Assert.AreEqual("this-name-is-used", (string) obj["log"]["name"]);
            Assert.AreEqual(ExpectedLogType, (string) obj["log"]["type"]);
            Assert.AreEqual(expectedContextId, (long) obj["properties"]["context_id"]);
        }

        [Test]
        public void LogError()
        {
            const string logName = "UnitTestLogError";
            const string exceptionMessage = "Not a real exception!";

            var sbt = ConfigureLogging();
            var logger = LogManager.GetLogger(logName);
            Assert.IsNotNull(logger);

            try
            {
                throw new NotSupportedException(exceptionMessage);
            }
            catch (Exception ex)
            {
#if LOG4NET
                logger.Error("Log4Net fake error " + DateTime.Now.Ticks, ex);
#else
                logger.Error(ex, "NLog fake error " + DateTime.Now.Ticks);
#endif
            }
            var logOutput = sbt.GetText();
            Debug.Write(logOutput);

            var obj = JsonConvert.DeserializeObject<JObject>(logOutput);
            Assert.AreEqual("Error", (string)obj["log"]["level"]);
            Assert.AreEqual(logName, (string)obj["log"]["name"]);
            Assert.AreEqual(ExpectedLogType, (string)obj["log"]["type"]);
            Assert.AreEqual(exceptionMessage, (string)obj["exception"]["message"]);
            Assert.IsNotNullOrEmpty((string)obj["exception"]["stacktrace"]);
        }

#if LOG4NET
        private static StringBuilderAppender ConfigureLogging()
        {
            var hierarchy = LogManager.GetRepository() as Hierarchy;
            Assert.IsNotNull(hierarchy);
            hierarchy.Root.Level = Level.All;

            var appender = new StringBuilderAppender();
            appender.Layout = new KibanaLayout(@"%logger (%property{ContextId}) [%level]- %message");
            appender.ActivateOptions();
            hierarchy.Root.AddAppender(appender);

            //mark repository as configured and  
            //notify that is has changed.  
            hierarchy.Configured = true;
            hierarchy.RaiseConfigurationChanged(EventArgs.Empty);
            return appender;
        }

        private sealed class StringBuilderAppender : AppenderSkeleton
        {
            private readonly StringBuilder _sb = new StringBuilder();

            protected override void Append(LoggingEvent loggingEvent)
            {
                _sb.Append(RenderLoggingEvent(loggingEvent));
            }

            public string GetText()
            {
                return _sb.ToString();
            }

        }
#else
        private static StringBuilderTarget ConfigureLogging()
        {
            var config = new LoggingConfiguration();

            var target = new StringBuilderTarget
            {
                Layout = new KibanaLayout(
                    @"${logger} ${gdc:item=correlation_id} ${mdc:item=context_id} [${level}]- ${message}",
                    "correlation_id", "context_id")
            };
            config.AddTarget("unittest", target);
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, target));
            LogManager.Configuration = config;
            return target;
        }

        private sealed class StringBuilderTarget : TargetWithLayout
        {
            private readonly StringBuilder _sb = new StringBuilder();

            protected override void Write(LogEventInfo logEvent)
            {
                _sb.Append(Layout.Render(logEvent));
            }

            public string GetText()
            {
                return _sb.ToString();
            }
        }
#endif
    }
}