﻿using System;
using System.Diagnostics;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using NUnit.Framework;
using Primordial.ELK.Test;
using NL = NLog;
using Logger = NLog.Logger;

namespace Primordial.ELK.NLog.Test
{
#if IGNORE_UNIT_TEST
    [Ignore("TeamCity is unable to connect to the remote host")]
#endif
    [TestFixture]
    public class ElasticsearchTargetTests
    {
        [Test]
        public void LogInfo()
        {
            Globals.SetApplicationAssembly<ElasticsearchTargetTests>();
            Configure(@"logstash-nlog.log");
            var logger = LogManager.GetCurrentClassLogger();
            Assert.IsNotNull(logger);
            logger.Info("Fake Info");
        }

        [Test]
        public void LogInfoAltIndex()
        {
            Globals.SetApplicationAssembly<ElasticsearchTargetTests>();
            Configure(@"logstash-nlog.log", "unittest");
            var logger = LogManager.GetCurrentClassLogger();
            Assert.IsNotNull(logger);
            logger.Info("Fake Info");
        }
        
        [Test]
        public void LogInfoAltIndexWithCorrelationId()
        {
            Globals.SetApplicationAssembly<ElasticsearchTargetTests>();
            Configure(@"logstash-nlog.log", "unittest");
            var logger = LogManager.GetCurrentClassLogger();
            Assert.IsNotNull(logger);

            NL.GlobalDiagnosticsContext.Set("correlation_id", Guid.NewGuid().ToString("N"));
            NL.MappedDiagnosticsContext.Set("context_id", (1337000L).ToString());

            logger.Info("Fake Info");
        }

        [Test]
        public void LogLevels()
        {
            Globals.SetApplicationAssembly<ElasticsearchTargetTests>();
            Configure(@"logstash-nlog.log");

            //var logger = LogManager.GetLogger("Test");
            var logger = LogManager.GetCurrentClassLogger();
            Assert.IsNotNull(logger);
            logger.Debug("Fake Debug");
            logger.Info("Fake Info");
            logger.Warn("Fake Warn");
            logger.Error("Fake Error");
            logger.Fatal("Fake Fatal");
        }

        [Test]
        public void LogError()
        {
            Configure(@"logstash-nlog.log");

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                //Debug.WriteLine("Logging warning: {0}", i);
                //logger.ErrorFormat("Fake Error {0}", i);
                //logger.Error("Fake Error " + i, new Excep);

                try
                {
                    throw new NotSupportedException("Not a real exception! " + i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error(ex, "Fake error " + i);
                }
            }
        }

        [Test]
        public void LogErrorFileConfig()
        {
            //Configure(@"logstash-nlog.log");
            LogManager.Configuration = new XmlLoggingConfiguration("NLogTest.config");
            Debug.WriteLine(LogManager.Configuration);

            foreach (var target in LogManager.Configuration.AllTargets)
            {
                Debug.Print("target={0}", target);
            }

            foreach (var loggingRule in LogManager.Configuration.LoggingRules)
            {
                Debug.Print("loggingRule={0}", loggingRule);
            }

            Logger logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            for (int i = 1; i <= 3; i++)
            {
                try
                {
                    throw new NotSupportedException("Not a real exception! #" + i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error(ex, "Fake error " + i);
                }
            }
        }

        [Test]
        public void LogEventInfoUsingFileConfig()
        {
            //Configure(@"logstash-nlog.log");
            LogManager.Configuration = new XmlLoggingConfiguration("NLogTest.config");
            Debug.WriteLine(LogManager.Configuration);

            foreach (var target in LogManager.Configuration.AllTargets)
            {
                Debug.Print("target={0}", target);
            }

            foreach (var loggingRule in LogManager.Configuration.LoggingRules)
            {
                Debug.Print("loggingRule={0}", loggingRule);
            }

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            //for (int i = 1; i <= 3; i++)
            //{
            //    var loggingEvent = new LogEventInfo(
            //        LogLevel.Info,
            //        "this-logger-name-is-used",
            //        "Just testing the logger with a warning. #" + i);
            //    loggingEvent.Properties["test_number"] = i;
            //    logger.Log(loggingEvent);
            //}
            {
                var loggingEvent = new LogEventInfo(
                    LogLevel.Info,
                    "this-logger-name-is-used",
                    "Just testing the logger with a warning.");
                loggingEvent.Properties["test_number"] = 666;
                logger.Log(loggingEvent);
            }
        }

        private static void Configure(string logFileName, string IndexName = "logstash")
        {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            var consoleTarget = new ConsoleTarget();
            config.AddTarget("console", consoleTarget);

            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            var elasticsearchTarget = new ElasticsearchTarget();
            if (!string.IsNullOrEmpty(IndexName))
            {
                elasticsearchTarget.Index = new SimpleLayout("unittest-${date:format=yyyy.MM.dd}");
            }
            config.AddTarget("elasticsearch", elasticsearchTarget);

            // Step 3. Set target properties 

            //const string layout = @"${date:format=HH\:MM\:ss} ${logger} ${message}";
            const string pattern = @"${message}${newline}${all-event-properties}";

            //consoleTarget.Layout = consoleTarget.Layout = layout;;

            //fileTarget.FileName = "${basedir}/${processname}.log";
            fileTarget.FileName = logFileName;
            fileTarget.Layout = new KibanaLayout(pattern,
                    new[] { "correlation_id", "context_id" });
            fileTarget.DeleteOldFileOnStartup = true;

            elasticsearchTarget.ElasticsearchUrl = TestSettings.ElasticsearchUrl;
            elasticsearchTarget.Layout = new KibanaLayout(pattern,
                    new[] { "correlation_id", "context_id" });

            // Step 4. Define rules
            var rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule1);

            var rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            var rule3 = new LoggingRule("*", LogLevel.Debug, elasticsearchTarget);
            config.LoggingRules.Add(rule3);

            // Step 5. Activate the configuration
            LogManager.Configuration = config;
        }
    }
}
