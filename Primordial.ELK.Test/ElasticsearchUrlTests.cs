﻿using System;
using System.Diagnostics;
using NUnit.Framework;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.Test
{
    [TestFixture]
    public class ElasticsearchUtilsTests
    {
        [Test]
        public void ElasticsearcIndexUrl()
        {
            var baseUri = new Uri("http://localhost:9200");
            Debug.Print("baseUri  = {0}", baseUri);

            Assert.AreEqual("http://localhost:9200/", baseUri.ToString());

            var elasticsearchClient = new ElasticsearchClient(baseUri, "someindex");

            var indexUri = elasticsearchClient.CreateElasticsearcIndexUrl();
            Debug.Print("indexUri = {0}", indexUri);

            indexUri = elasticsearchClient.CreateElasticsearcIndexUrl("someindex", "sometype", "someid");
            Debug.Print("indexUri = {0}", indexUri);
            Assert.AreEqual("http://localhost:9200/someindex/sometype/someid", indexUri.ToString());
        }
    }
}