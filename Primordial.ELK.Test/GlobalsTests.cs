﻿using NUnit.Framework;

namespace Primordial.ELK.Test
{
    [TestFixture]
    public class GlobalsTests
    {
        [Test]
        public void Defaults()
        {
            Assert.AreEqual(9200, Globals.DefaultElasticsearchPort);
            Assert.AreEqual(6379, Globals.DefaultRedisPort);
        }
    }
}
