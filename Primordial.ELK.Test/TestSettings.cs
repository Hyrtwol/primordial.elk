﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Primordial.ELK.Test
{
    internal static class TestSettings
    {
        private const string UserSettings = @"%USERPROFILE%\Documents\Primordial\TestSettings.txt";

        public static string ElasticsearchUrl
        {
            get
            {
                string url = null;
                var file = Environment.ExpandEnvironmentVariables(UserSettings);
                if (File.Exists(file))
                {
                    url = File.ReadLines(file).Select(l => l.Trim(' ')).FirstOrDefault(l => !l.StartsWith("//"));
                }
                if (string.IsNullOrWhiteSpace(url))
                {
                    url = "http://localhost:9200";
                }
                Debug.WriteLine("Unit test elasticsearch url: "+ url);
                return url;
            }
        }
    }
}
