﻿using System;
using System.Diagnostics;
using log4net.Appender;
using log4net.Core;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.log4net
{
    public class ElasticsearchAppender : AppenderSkeleton
    {
        public string ElasticsearchUrl { get; set; }

        public string IndexName { get; set; }
        public string IndexFormat { get; set; }

        public ElasticsearchAppender()
        {
            IndexName = "logstash";
            IndexFormat = "{0}-{1:yyyy.MM.dd}";
        }
        
        protected override void OnClose()
        {
            Debug.WriteLine("ElasticsearchAppender.OnClose()");
            base.OnClose();
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (loggingEvent == null) throw new ArgumentNullException(nameof(loggingEvent));
            var json = this.RenderLoggingEvent(loggingEvent);
            var elasticsearchUri = ElasticsearchClient.CreateUriFromUrl(ElasticsearchUrl);

            string indexName = IndexName;
            if (string.IsNullOrEmpty(indexName))
            {
                indexName = string.Format(IndexFormat, IndexName, DateTime.UtcNow);
            }
            if (string.IsNullOrEmpty(indexName))
            {
                indexName = string.Format("logstash-{0:yyyy.MM.dd}", DateTime.UtcNow);
            }

            var elasticsearchClient = new ElasticsearchClient(elasticsearchUri, indexName);
            elasticsearchClient.SendMessage(json);
        }
    }
}
